import * as $ from 'jquery';
import Post from '@models/Post';
import json from './assets/json.json';
import './babel'

import NatureImg from './assets/2.jpg'
import './styles/styles.css';
import './styles/less.less';

const post = new Post('Webpack Post Title', NatureImg);

$('.pre').html(post.toString())

console.log(post.toString());
console.log(`JSON:`, json);

const unused = 42;

